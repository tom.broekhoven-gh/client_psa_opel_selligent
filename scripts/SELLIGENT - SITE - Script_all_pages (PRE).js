/**
 * NAME: SELLIGENT - SITE - Script_all_pages
 * Basic selligent site script: 
 *  ENV: Pre-production
 *  loading the selligent script commands
 *  initiating queue:
 *      async => false,
        isEvent => false
        isTrack => true
        isTargeting => true
 * 
 *  ! Scripts needs to be triggered on all sites
 */

<script>
if (wa === undefined) {
  var wa = document.createElement("script"), 
      wa_s = document.getElementsByTagName("script")[0]; 
  wa.src = "https://targetp.emsecure.net/Content/69b89fb5-d21e-431c-8684-8e1fa606ca89/69b89fb5d21e431c86848e1fa606ca89_1.js"; 
  wa.type = "text/javascript"; 
  wa_s.parentNode.insertBefore(wa, wa_s); 
  wa.bt_queue = []; 
  wa.afterInit = function()
  {
  wa.bt_queue.push({
  "async": false,
  "isEvent": false,
  "isTrack": true,
  "isTargeting": true
  });
  }
}
</script>