# Opel Selligent documentation

## Tag Manager Container

<details>

A selligent respository with selligent only items has been created to provide the triggers **scripts** & **variables**.
The workspace is available as a ***.JSON*** file.  
**[Link to the workspace JSON](https://gitlab.com/tom.broekhoven/client_psa_opel_selligent/-/blob/9c4b3d03e20533a009b1162382c4ed12833bc1cd/scripts/Opel_Selligent_Workspace.json)**  

This container can be imported with these simple steps.

**From a tag manager overview / admin area**
1. Select the container (you want data to be added)
2. Select import container
3. Select / create a new workspace
4. Pick merged as a option (overwrite would overwrite the whole container setup)

**Importing the container**
![Import your selligent container](img/import-your-container.png "Import your selligent container")

</details>

***
<br>

## Tag Templates  
***
<br>

### SELLIGENT - SITE - Script_all_pages
<details>

<summary>Click to view tag details</summary>
Custom HTML script to load the selligent base script plus initiation of config items. 

**Javascript Template**
``` javascript
if (wa === undefined) {
  var wa = document.createElement("script"), 
      wa_s = document.getElementsByTagName("script")[0]; 
  wa.src = "https://targetp.emsecure.net/Content/69b89fb5-d21e-431c-8684-8e1fa606ca89/69b89fb5d21e431c86848e1fa606ca89_1.js"; 
  wa.type = "text/javascript"; 
  wa_s.parentNode.insertBefore(wa, wa_s); 
  wa.bt_queue = []; 
  wa.afterInit = function()
  {
  wa.bt_queue.push({
  "async": false,
  "isEvent": false,
  "isTrack": true,
  "isTargeting": true
  });
  }
}
```

**Tag visualization**  
![SELLIGENT - SITE - Script_all_pages](img/tag_customhtml_selligen-site.png "SELLIGENT - SITE - Script_all_pages")

**Triggers**  
* page - all pages - opel.nl
* Exclude - Consent COM  
* page - hostname - pre.opel.nl 
   
</details>

***

### Custom HTML - Selligent Debug
<details>
<summary>Click to view tag details</summary>
Custom HTML script to provide console logging in Google Tag Manager Dubug mode.
Function on the script is to print selligent opject parameters by page.
These parameter are supported.

**Javascript Template**
``` javascript
  var _selObj = {};
  _selObj.AANDRIJVING = {{regex table - AANDRIJVING}};
  _selObj['AUTO CATEGORIE'] = {{regex table - AUTO CATEGORIE}};
  _selObj.BODYSTYLE = {{regex table - BODYSTYLE}};
  _selObj.CTA = {{regex table - CTA}};
  _selObj['INTERESSE SUBTYPE'] = {{regex table - INTERESSE SUBTYPE}};
  _selObj.INTERESSES = {{regex table - INTERESSES}};
  _selObj.MOI = {{regex table - MOI}};
  _selObj['OPEL BEZIT'] = {{regex table - OPEL BEZIT}};
  _selObj['SOORT AUTO'] = {{regex table - SOORT AUTO}};
  _selObj.TOUCHPOINTS = {{regex table - TOUCHPOINTS}};
  _selObj['TYPE RIJDER'] = {{regex table - TYPE RIJDER}};
  console.table(_selObj);
```
**Tag visualization**
![Tag - Custom HTML - Selligent Debug](img/tag_customhtml_selligent-debug.png "Tag - Custom HTML - Selligent Debug")

**Triggers**
* Pageview - Debug  
  
**Variables**: 
* regex table - AANDRIJVING
* regex table - AUTO CATEGORIE
* regex table - BODYSTYLE
* regex table - CTA
* regex table - INTERESSE SUBTYPE
* regex table - INTERESSES

</details>

***

### SELLIGENT - Iframe listener - All Pages
<details>
<summary>Click to view tag details</summary>
Custom HTML script that enables listening to postmessages from the gm.emsecure.net domain.
Will accept messages and will take the data from the message translating it into datalayer events.

**Javascript Template**
``` javascript
window.addEventListener('message', function(e) {
    if (e.origin === 'https://gm.emsecure.net') {
        evObj = e.data[0]
        window.dataLayer.push(evObj);
    }
}, false);
```

**Tag visualization**
![SELLIGENT - Iframe listener - All Pages](img/tag_customhtml_iframe-listener.png "SELLIGENT - Iframe listener - All Pages")

**Triggers**
* All Pages

</details>

***

### GOOGLE ANALYTICS - Event - Selligent Interactions
<details>
<summary>Click to view tag details</summary>

Measuring all triggered selligent interactions. Selligent needs to push those items through the **selligent** custom event command.

**Tag visualization**
![GOOGLE ANALYTICS - Event - Selligent Interactions](img/tag_ga_selligent-interactions.png "GOOGLE ANALYTICS - Event - Selligent Interactions")

**Triggers**
* Custom Event - Selligent
* Exclude - Consent ANA

**Variables**
* dl - eCategory
* dl - eAction
* dl - eLabel
* dl - eMoi
* cj - selligent - type of action
</details>

***

### SELLIGENT - SITE - digitalAudience
<details>
<summary>Click to view tag details</summary>
Measuring events when the BT is not undefined.

**Javascript Template**
``` javascript
x = new XMLHttpRequest();x.open('POST','https://sitep.slgnt.eu/api/track');
x.setRequestHeader('Content-Type','application/json');
x.onload = function(){if(x.status === 200){var j=JSON.parse(x.responseText);var dat=document.getElementById('dasynctag');
if(dat){dat.remove();};var u='https://target.digitalaudience.io/bakery/sync/p?dac='+j.$$profileId+'&dap=ssite1';
var sa=document.createElement('img');sa.src=u;sa.id='dasynctag';sa.style.height='1px';sa.style.width='1px';sa.style.display='none';
document.getElementsByTagName('body')[0].appendChild(sa);if(!j.profileInfo.isCRMIdentified){
    var ur='https://gm.emsecure.net/renderers/content/Json/fM1eTyE0dcnyxlpoqY7g4kZ%2BJ7MxNCbv1LMw7VarnxuvkKuMRypR4QOJKCWxSeohBfJyR2NfRy5If7?PROFILEID='+j.$$profileId;var xhr = new XMLHttpRequest();xhr.open('GET', ur);
    xhr.onload = function(){if (xhr.status === 200){
        var resp = JSON.parse(xhr.responseText);
        var campUid = resp.Body;
        if(campUid.length>0 && campUid != 0){wa.bt_queue.push({"customIdentifier": campUid,"async":false,"isEvent":true,"isTrack":true,"isTargeting":true});}}};xhr.send();};}};
        var d = JSON.stringify({universeId:"69b89fb5-d21e-431c-8684-8e1fa606ca89",profileId:BT.getProfileId(),url:"https://www.opel.nl"});
    x.send(d);
```

**Tag visualization**  
![SELLIGENT - SITE - digitalAudience](img/tag_selligent-site-digitalaudience.png "SELLIGENT - SITE - digitalAudience")

**Triggers**
* event - all pages - bt not undefined
* Exclude - Consent ANA
</details>

***

### SELLIGENT - SITE - Selligent_details-variables
<details>
<summary>Click to view tag details</summary>

**Javascript Template**
``` javascript
wa.bt_queue.push({
    "tags": [{
        "tag": "QUANTUM_AANDRIJVING",
        "value": {{regex table - AANDRIJVING}}
    }, {
        "tag": "QUANTUM_AUTO_CATEGORIE",
        "value": {{regex table - AUTO CATEGORIE}}
    }, {
        "tag": "QUANTUM_CTA",
        "value": {{regex table - CTA}}
    }, {
        "tag": "QUANTUM_INTERESSES",
        "value": {{regex table - INTERESSES}}
    }, {
        "tag": "QUANTUM_INTERESSE_SUBTYPE",
        "value": {{regex table - INTERESSE SUBTYPE}}
    }, {
        "tag": "QUANTUM_MOI",
        "value": {{regex table - MOI}}
    }, {
        "tag": "QUANTUM_BODY_STYLE",
        "value": {{regex table - BODYSTYLE}}
    }, {
        "tag": "QUANTUM_OPEL_BEZIT",
        "value": {{regex table - OPEL BEZIT}}
    }, {
        "tag": "QUANTUM_SOORT_AUTO",
        "value": {{regex table - SOORT AUTO}}
    }, {
        "tag": "QUANTUM_TYPE_RIJDER",
        "value": {{regex table - TYPE RIJDER}}
    }, {
        "tag": "QUANTUM_URL",
        "value": {{js - Full URL}}
    }, {
        "tag": "QUANTUM_TOUCHPOINTS",
        "value": {{regex table - TOUCHPOINTS}}
    }],
    "isEvent": true,
    "isTargeting": true
})
```

**Tag visualization**
![SELLIGENT - SITE - Selligent_details-variables](img/tag_selligent-details-variables-overall.png "SELLIGENT - SITE - Selligent_details-variables")

**Triggers**
* click - form - brochure complete
* event - dealer search
* history - config - step 3 - overview
* Exclude - Consent COM

**Variables**
* js - Full URL 
* regex table - AANDRIJVING
* regex table - AUTO CATEGORIE
* regex table - CTA
* regex table - INTERESSES
* regex table - INTERESSE SUBTYPE
* regex table - MOI
* regex table - MOI
* regex table - OPEL BEZIT
* regex table - SOORT AUTO
* regex table - TYPE RIJDER
* regex table - TOUCHPOINTS

</details>

***

### SELLIGENT - SITE - Selligent_details-variables click based
<details>
<summary>Click to view tag details</summary>

``` javascript
wa.bt_queue.push({
    "tags": [{
        "tag": "QUANTUM_AANDRIJVING",
        "value": {{regex table - AANDRIJVING}}
    }, {
        "tag": "QUANTUM_AUTO_CATEGORIE",
        "value": {{regex table - AUTO CATEGORIE}}
    }, {
        "tag": "QUANTUM_CTA",
        "value": {{regex table - CTA}}
    }, {
        "tag": "QUANTUM_INTERESSES",
        "value": {{regex table - INTERESSES}}
    }, {
        "tag": "QUANTUM_INTERESSE_SUBTYPE",
        "value": {{regex table - INTERESSE SUBTYPE}}
    }, {
        "tag": "QUANTUM_MOI",
        "value": {{regex table - MOI}}
    }, {
        "tag": "QUANTUM_OPEL_BEZIT",
        "value": {{regex table - OPEL BEZIT}}
    }, {
        "tag": "QUANTUM_SOORT_AUTO",
        "value": {{regex table - SOORT AUTO}}
    }, {
        "tag": "QUANTUM_TYPE_RIJDER",
        "value": {{regex table - TYPE RIJDER}}
    }, {
        "tag": "QUANTUM_URL",
        "value": {{js - Full URL}}
    }, {
        "tag": "QUANTUM_TOUCHPOINTS",
        "value": {{regex table - TOUCHPOINTS - click based}}
    }],
    "isEvent": true,
    "isTargeting": true
})

function reqListener() {
    var e = JSON.parse(this.responseText);
    e = e.$$profileId;
    var f = {{regex table - MOI}}
    var t = {{regex table - TOUCHPOINTS - click based}}
    e = "https://gm.emsecure.net/renderers/content/Json/Ji6f%2BPM6Hq1bBuh7Wv3BwRTyC95Y1Lx9bWaHNG0_JAPafaw5E3TC8%2BGOxLkmThdmj_DaYxMeGwg4Jh?SITEID=" + e + "&TOUCHPOINT=" + t + "&MOI=" + f, (t = new XMLHttpRequest).open("POST", e), t.send()
}
var api = "https://sitep.slgnt.eu/api/track",
    oReq = new XMLHttpRequest;
oReq.addEventListener("load", reqListener), oReq.open("POST", api), oReq.send(JSON.stringify({
    universeId: "69b89fb5-d21e-431c-8684-8e1fa606ca89",
    profile: null,
    profileId: BT.getProfileId(),
    url: "https://www.opel.nl",
    referer: "",
    tagValues: [],
    exposedFields: [{
        field: "Profile_id"
    }],
    allowCookies: !0,
    allowLocalStorage: !1,
    pageTitle: "",
    metaTags: {}
}));
``` 

**Tag visualization**
![SELLIGENT - SITE - Selligent_details-variables click based](img/tag_selligent-details-variables-clicks.png "SELLIGENT - SITE - Selligent_details-variables click based")

**Triggers**  
* link click - change finance type brandpage
* link click - downloads
* Exclude - Consent COM

**Variables**
* js - Full URL 
* regex table - AANDRIJVING
* regex table - AUTO CATEGORIE
* regex table - CTA
* regex table - INTERESSES
* regex table - INTERESSE SUBTYPE
* regex table - MOI
* regex table - MOI
* regex table - OPEL BEZIT
* regex table - SOORT AUTO
* regex table - TYPE RIJDER
* regex table - TOUCHPOINTS 

</details>

***

### SELLIGENT - SITE - Selligent_details-variables url based
<details>
<summary>Click to view tag details</summary>

```javascript
wa.bt_queue.push({
    "tags": [{
        "tag": "QUANTUM_AANDRIJVING",
        "value": {{regex table - AANDRIJVING}}
    }, {
        "tag": "QUANTUM_AUTO_CATEGORIE",
        "value": {{regex table - AUTO CATEGORIE}}
    }, {
        "tag": "QUANTUM_CTA",
        "value": {{regex table - CTA}}
    }, {
        "tag": "QUANTUM_INTERESSES",
        "value": {{regex table - INTERESSES}}
    }, {
        "tag": "QUANTUM_INTERESSE_SUBTYPE",
        "value": {{regex table - INTERESSE SUBTYPE}}
    }, {
        "tag": "QUANTUM_MOI",
        "value": {{regex table - MOI}}
    }, {
        "tag": "QUANTUM_OPEL_BEZIT",
        "value": {{regex table - OPEL BEZIT}}
    }, {
        "tag": "QUANTUM_SOORT_AUTO",
        "value": {{regex table - SOORT AUTO}}
    }, {
        "tag": "QUANTUM_TYPE_RIJDER",
        "value": {{regex table - TYPE RIJDER}}
    }, {
        "tag": "QUANTUM_URL",
        "value": {{js - Full URL}}
    }, {
        "tag": "QUANTUM_TOUCHPOINTS",
        "value": {{regex table - TOUCHPOINTS - url based}}
    }],
    "isEvent": true,
    "isTargeting": true
})
function reqListener() {
    var e = JSON.parse(this.responseText);
    e = e.$$profileId;
    var f = {{regex table - MOI}}
    var t = {{regex table - TOUCHPOINTS - url based}}
    e = "https://gm.emsecure.net/renderers/content/Json/Ji6f%2BPM6Hq1bBuh7Wv3BwRTyC95Y1Lx9bWaHNG0_JAPafaw5E3TC8%2BGOxLkmThdmj_DaYxMeGwg4Jh?SITEID=" + e + "&TOUCHPOINT=" + t + "&MOI=" + f, (t = new XMLHttpRequest).open("POST", e), t.send()
}
var api = "https://sitep.slgnt.eu/api/track",
    oReq = new XMLHttpRequest;
oReq.addEventListener("load", reqListener), oReq.open("POST", api), oReq.send(JSON.stringify({
    universeId: "69b89fb5-d21e-431c-8684-8e1fa606ca89",
    profile: null,
    profileId: BT.getProfileId(),
    url: "https://www.opel.nl",
    referer: "",
    tagValues: [],
    exposedFields: [{
        field: "Profile_id"
    }],
    allowCookies: !0,
    allowLocalStorage: !1,
    pageTitle: "",
    metaTags: {}
}));
```

**Tag visualization**
![SELLIGENT - SITE - Selligent_details-variables url based](img/tag_selligent-details-variables-urlbased.png "SELLIGENT - SITE - Selligent_details-variables url based")

**Triggers**
* click - form - brochure complete
* event - dealer search
* history - config - step 3 - overview
* history - config - step 3 - overview

**Variables**
* js - Full URL 
* regex table - AANDRIJVING
* regex table - AUTO CATEGORIE
* regex table - CTA
* regex table - INTERESSES
* regex table - INTERESSE SUBTYPE
* regex table - MOI
* regex table - MOI
* regex table - OPEL BEZIT
* regex table - SOORT AUTO
* regex table - TYPE RIJDER
* regex table - TOUCHPOINTS 
</details>

***

### SELLIGENT - SITE - Touchpoint tag
<details>
<summary>Click to view tag details</summary>

```javascript
function reqListener() {
    var e = JSON.parse(this.responseText);
    e = e.$$profileId;
    var f = {{regex table - MOI}}
    var t = {{regex table - TOUCHPOINTS}}
    e = "https://gm.emsecure.net/renderers/content/Json/Ji6f%2BPM6Hq1bBuh7Wv3BwRTyC95Y1Lx9bWaHNG0_JAPafaw5E3TC8%2BGOxLkmThdmj_DaYxMeGwg4Jh?SITEID=" + e + "&TOUCHPOINT=" + t + "&MOI=" + f, (t = new XMLHttpRequest).open("POST", e), t.send()
}
var api = "https://sitep.slgnt.eu/api/track",
    oReq = new XMLHttpRequest;
oReq.addEventListener("load", reqListener), oReq.open("POST", api), oReq.send(JSON.stringify({
    universeId: "69b89fb5-d21e-431c-8684-8e1fa606ca89",
    profile: null,
    profileId: BT.getProfileId(),
    url: "https://www.opel.nl",
    referer: "",
    tagValues: [],
    exposedFields: [{
        field: "Profile_id"
    }],
    allowCookies: !0,
    allowLocalStorage: !1,
    pageTitle: "",
    metaTags: {}
}));
```

**Tag visualization**

tag_selligent-touchpoints.png

**Triggers**
* event - all pages - touchpoint not undefined
* Exclude - Consent COM

**Variables** 

</details>

***


<br><br>
## Variables
List of all variables used in selligent context & projects.

#### regex table - AANDRIJVING
<details>

**Name**: regex table - AANDRIJVING   
**Type**: Regex Table  
**Visual**:   
![regex table - AANDRIJVING](img/regex-table-AANDRIJVING.png  "regex table - AANDRIJVING")  

</details>

***

#### regex table - AUTO CATEGORIE
<details>

**Name**: regex table - AUTO CATEGORIE  
**Type**: Regex Table  
**Visual**:    
![regex table - AUTO CATEGORIE](img/regex-table-AUTOCATEGORIE.png  "regex table - AUTO CATEGORIE") 

</details>

***

#### regex table - BODYSTYLE
<details>

**Name**: regex table - BODYSTYLE  
**Type**: Regex Table  
**Visual**:    
![regex table - BODYSTYLE](img/regex-table-BODYSTYLE.png  "regex table - BODYSTYLE") 

</details>

***

#### regex table - CTA
<details>

**Name**: regex table - CTA  
**Type**: Regex Table  
**Visual**:  
![regex table - CTA](img/regex-table-CTA.png  "regex table - CTA")   

</details>

***

#### regex table - INTERESSE SUBTYPE
<details>

**Name**: regex table - INTERESSE SUBTYPE  
**Type**: Regex Table  
**Visual**:  
![regex table - CTA](img/regex-table-INTERESSESUBTYPE.png  "regex table - INTERESSE SUBTYPE")  

</details>

***

#### regex table - INTERESSES
<details>

**Name**: regex table - INTERESSES    
**Type**: Regex Table  
**Visual**:    
![regex table - INTERESSES](img/regex-table-INTERESSES.png  "regex table - INTERESSES")  

</details>

***

#### regex table - MOI
<details>

**Name**: regex table - MOI  
**Type**: Regex Table  
**Visual**:    
![regex table - MOI](img/regex-table-MOI.png  "regex table - MOI")  

</details>

***

#### regex table - OPEL BEZIT
<details>

**Name**: regex table - OPEL BEZIT  
**Type**: Regex Table  
**Visual**:    
![regex table - OPEL BEZIT](img/regex-table-OPELBEZIT.png  "regex table - OPEL BEZIT")  

</details>

***

#### regex table - SOORT AUTO
<details>

**Name**: regex table - SOORT AUTO  
**Type**: Regex Table  
**Visual**:    
![regex table - SOORT AUTO](img/regex-table-SOORTAUTO.png  "regex table - SOORT AUTO") 

</details>

***

#### regex table - TOUCHPOINTS
<details>

**Name**: regex table - TOUCHPOINTS  
**Type**: Regex Table  
**Visual**:    
![regex table - TOUCHPOINTS](img/regex-table-TOUCHPOINTS.png  "regex table - TOUCHPOINTS") 

</details>

***

#### regex table - TYPE RIJDER
<details>

**Name**: regex table - TYPE RIJDER  
**Type**: Regex Table  
**Visual**:  
![regex table - TYPE RIJDER](img/regex-table-TYPERIJDER.png  "regex table - TYPE RIJDER")   

</details>

***

#### dl - eCategory
<details>

**Name**: dl - eCategory  
**Type**: dataLayer  
**Visual**:  
![dl - eCategory](img/dl-eCategory.png  "dl - eCategory")   

</details>

***

#### dl - eAction
<details>

**Name**: dl - eAction  
**Type**: dataLayer  
**Visual**:  
![dl - eAction](img/dl-eAction.png  "dl - eAction")  

</details>

***

#### dl - eLabel
<details>

**Name**: dl - eLabel  
**Type**: dataLayer  
**Visual**:  
![dl - eLabel](img/dl-eLabel.png  "dl - eLabel")   

</details>

***

#### dl - eMoi
<details>

**Name**: dl - eMoi  
**Type**: dataLayer  
**Visual**:  
![dl - eMoi](img/dl-eMoi.png  "dl - eMoi")   

</details>

***

#### cj - selligent - type of action
<details>

**Name**: cj - selligent - type of action
**Type**: Custom Javascript  
**Visual**:  
![cj - selligent - type of action](img/cj-selligent-actiontype.png  "cj - selligent - type of action")   

</details>

***

#### 1st-Party Cookie - _psac_gdpr_consent_purposes
<details>

**Name**: 1st-Party Cookie - _psac_gdpr_consent_purposes
**Type**: Cookie  
**Visual**:  
![1st-Party Cookie - _psac_gdpr_consent_purposes](img/cookie_psac_gdpr_consent_purposes.png  "1st-Party Cookie - _psac_gdpr_consent_purposes")     

</details>

***

<br><br>

## Triggers

### All Pages
<details>

**Name**: All Pages   
**Type**: Page View  
**Visual**:    
![All Pages](img/triggers_allpages.png "All Pages")

</details>

***

### click - form - brochure complete
<details>

**Name**: click - form - brochure complete 
**Type**: Click   
**Visual**:    
![](img/triggers_brochure-complete.png "")

</details>

***

### Custom Event - Selligent

<details>

**Name**: Custom Event - Selligent  
**Type**: Custom Event  
**Visual**:    
![Custom Event - Selligent](img/triggers_event-selligent.png "Custom Event - Selligent")

</details>

***

### event - all pages - bt not undefined

<details>

**Name**: event - all pages - bt not undefined 
**Type**: Custom event  
**Visual**:    
![](img/triggers_pageview_bt-not-undefined.png "")

</details>

***

### event - all pages - touchpoint not undefined

<details>

**Name**: event - all pages - touchpoint not undefined  
**Type**: Custom Event   
**Visual**:    
![](img/triggers_event-touchpoint-not-undefined.png "")

</details>

***

### event - dealer search

<details>

**Name**: event - dealer search  
**Type**: Custom Event   
**Visual**:    
![event - dealer search](img/triggers_event_dealer-search.png "event - dealer search")

</details>

***

### event - updatevirtualpath - config start

<details>

**Name**: event - updatevirtualpath - config start  
**Type**: Custom Event   
**Visual**:    
![event - updatevirtualpath - config start](img/triggers_event_updatevirtualpath-config-start.png "event - updatevirtualpath - config start")

</details>

***

### Exclude - Consent ANA

<details>

**Name**: Exclude - Consent ANA 
**Type**: Custom Event  
**Visual**:    
![Exclude - Consent ANA](img/triggers_exclude-consent-analytics.png "Exclude - Consent ANA")

</details>

***

### Exclude - Consent COM

<details>

**Name**: Exclude - Consent COM 
**Type**: Custom Event   
**Visual**:    
![Exclude - Consent COM](img/triggers_exclude_consent-com.png "Exclude - Consent COM")

</details>

***

### history - config - step 3 - overview

<details>

**Name**: history - config - step 3 - overview  
**Type**: History   
**Visual**:    
![history - config - step 3 - overview](img/triggers_history_step-3.png "history - config - step 3 - overview")

</details>

***

### link click - change finance type brandpage

<details>

**Name**: link click - change finance type brandpage  
**Type**: Link Click   
**Visual**:    
![link click - change finance type brandpage](img/triggers_linkclick-change-finance.png "link click - change finance type brandpage")

</details>

***

### link click - downloads

<details>

**Name**: link click - downloads  
**Type**: Link Click  
**Visual**:    
![link click - downloads](img/triggers_linkclick-downloads.png "link click - downloads")

</details>

***

### page - all pages - opel.nl

<details>

**Name**: triggers_page-all-pages-opel.png  
**Type**: Page View   
**Visual**:    
![page - all pages - opel.nl](img/triggers_page-all-pages-opel.png "page - all pages - opel.nl")

</details>

***

### page - hostname - pre.opel.nl

<details>

**Name**: page - hostname - pre.opel.nl  
**Type**: Page View  
**Visual**:    
![page - hostname - pre.opel.nl](img/triggers_pageview_hostname-pre-opel.png "page - hostname - pre.opel.nl")

</details>

***

### Pageview - Debug

<details>

**Name**: Pageview - Debug   
**Type**: Page View  
**Visual**:    
![Pageview - Debug pageview](img/triggers_pageview_debug.png "Pageview - Debug")

</details> 
